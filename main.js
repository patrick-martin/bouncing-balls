// setup canvas

var canvas = document.querySelector('canvas');
var ctx = canvas.getContext('2d');

var width = canvas.width = window.innerWidth; //gets width of Window object in browswer
var height = canvas.height = window.innerHeight; //gets height of window object 

// function to generate random number

function random(min, max) {
    var num = Math.floor(Math.random() * (max - min)) + min;
    return num;
}

//Start by creating a constructor function
//remember constructors start w/ capital letters; 'this' will refer to object inheriting from prototype
//arguments passed in will be the given values of the object
function Ball(x, y, velX, velY, color, size) {
    this.x = x; //represents where our ball will lie on the horizontal axis
    this.y = y;
    this.velX = velX; //Will be added dynamically to the x-coordinate to generate movement when we animate the balls
    this.velY = velY; //
    this.color = color; //sets the color of each ball
    this.size = size; //this will be the radius, in pixels
}

//Adding the draw() method to our prototype-- must include constructor object name.prototype, then name of method we are adding
Ball.prototype.draw = function() {
        ctx.beginPath(); //states we want to begin drawing on our canvas 
        ctx.fillStyle = this.color; //we want our circle to have the color that we set the object to within the argument
        ctx.arc(this.x, this.y, this.size, 0, 2 * Math.PI); //.arc determines path of our bouncing circle.  
        ctx.fill(); //states "Finish drawing path we started w/ .beginPath()"
        //..'and fill() the area it takes up with the color we specified in fillStyle
    }
    //Now lets test our bouncing ball..
    //calling our object's draw() method within the console actually starts to draw the ball
    //this draws the ball, but now we want to bounce the ball...

//Adding an update method to our Ball prototype
Ball.prototype.update = function() {
        if ((this.x + this.size) >= width) {
            this.velX = -(this.velX)
        }
        if ((this.x - this.size) <= 0) {
            this.velX = -(this.velX);
        }
        if ((this.y + this.size) >= height) {
            this.velY = -(this.velY);
        }
        if ((this.y - this.size) <= 0) {
            this.velY = -(this.velY);
        }

        this.x += this.velX; //this generates movement.  Adding velX to the x-value moves the ball horizontally
        this.y += this.velY;
    }
    //Now lets start animating the balls and adding other balls..
    //first, we need a place to store all of our balls, and then populate it..
    //so we create an empty array to store our balls
var balls = []; //empty array to serve as bucket to contain all our ball objects
while (balls.length < 25) {
    var size = random(10, 20);
    var ball = new Ball( //creates new instance of Ball..
        random(0 + size, width - size),
        random(0 + size, height - size),
        random(-7, 7),
        random(-7, 7),
        'rgb(' + random(0, 255) + ',' + random(0, 255) + ',' + random(0, 255) + ')',
        size, //forgot this last 'size' parameter!! Wasn't working before.
    );

    balls.push(ball); //pushes new object into empty balls array
}

//all animation programs/games have some sort of loop (as in our while loop above)
//the while loop creates a new instance of our Ball constructor Class
//will have random velocity, random x,y starting coordinates, and random color
//our ball instance will be pushed into our empty 'balls' array
//once our array hits 25, the loop stops generating balls

function loop() {
    ctx.fillStyle = 'rgba(0,0,0,0.25)';
    ctx.fillRect(0, 0, width, height);

    for (var i = 0; i < balls.length; i++) {
        balls[i].draw(); //draws the balls as outlined in draw() method above
        balls[i].update(); //adds animation to the balls so they bounce around
    }

    requestAnimationFrame(loop); //the 'loop' function will call itself recurcsively
    //will run the function a set number of times a second so that it runs smoothly
}
loop(); //remember, we have to call the function for the balls to getting added to the screen























//Using this for demo [june 24 & 25]

//wanting to create a User prototype function (User class)
//User should have: userName; givenName; surname; email; lastLogIn
//User should be able to login, logout, and changePassword
//capital U -- function is called a 'constructor'--represents a class
// function User(userName, givenName, surname, email, lastLogIn){
//     this.username = username;
//     this.givenName = givenName;
//     this.surname = surname;
//     this.email = email;
//     this.isLoggedIn = false;
//     this.lastLogIn = lastLogIn;
//     this.lastLogOut = lastLogOut;
//     this.password = password //we never want to store our passwords like this (we want or passwords to be encoded/encrypted)
//         //we want it to be more like:
//         this.passwordHash = Cryptoservice.encryption(password)
//         //takes our password and encrypts it; returns encrypted pw that is stored
//         //this all takes place on server side.  This is an examination of server side scripting
// }

// const derekJimRick = new User(derekJimRick, Derek derekJimRick, Rick, djm@outlook.com)

// //when we use 'new' keyword on an object, we are creating our own baby object off of the prototype we are using the 'new' keyword on
// //this = {}; //creates a blank instance of this userClass

// //want a this.login method for our Class (our User prototype)
// User.prototype.login = function (){
//     //this
//     const success = UserAuthService.doLogin(this) //creates instance which is referred to when using 'this' keyword
//         if (success) 
//             this.lastLogIn = Date.now();
//             this.isLoggedIn = true; //added isLoggedIn property to our constructor, and added the 'true' boolean statement here.
// }

// User.prototype.logout = function(){}
// User.prototype.changePassword = function(password){
//     this.passwordHash = Cryptoservice.encryption(password)
//     if (this.passwordHash) return 'Success' //if we get something truthy (like a hash), then we return success
//     return new Error('Password change failed.  Make sure you have atleast 9 characters and at least one alphabetical character, and special character')
//         //this return Error serves as our 'else' statement-- NOTE we dont always have to have an 'else' statement
// }

// //problem with creating objects from our constructor is:
// //it has too many arguments that are being passed in
// //it is very difficult to tell what those arguments represent
// //we want something like:
// const derekJimRick = new User({
//     userName: 'DerekJimRick',
//     givenName: 'Derek Jim',
//     surname: 'Rick',
//     email: 'djr@outlook.com',
//     password: 'beyonceHasRickets'
// })
//in this instance, we are passing in ONE thing: an object literal w/ keys representing the property arguemnts, and the values being the actual values of arguments being passed in
//so we want to wrap arguments within constructor function into an object w/ curly braces
//just add {} within the parameter of User --called parameter destructuring (also object destructuring)


//DMG DEMO--June 25th
//remember; if there are more than 4 positional arguments a function/object instance takes:
    //make an 'options' argument!  
    //as in:
    const options = {
        userName: 'DerekJimRick',
        givenName: 'Derek Jim',
            surname: 'Rick',
             email: 'djr@outlook.com',
             password: 'beyonceHasRickets'
             
         }

         //and then plugging that into your function arguments
        
         const derekJimRick = new User(options) //options object created above is passed in as argument

         //property name lookup:
         //looks at instance object; if property isn't foound, looks at prototype; if not there; looks at ITS protptyople; etc.
         //null > Object[root Object] > Class - Instance > Class - Instance > Class > Instance Object
//in this case, our prototype is User.prototype > derekJimRick is our Object instance
         //we want to include types of Users; a Class in which User is instantiated from
        //derekJimRick <= Standard.User.prototype <= User.Prototype

//in big projects; we have multiple files (not just those in the top director)
         //we will have folders with files containing 'classes', 'services', etc.
            //splitting up our User Class into User.js
                //then adding services folder, containing 'CryptoService.js'
                    //..and 'UserAuthService.js'
         //then we HAVE to be sure to include these script files within our index.html page
    //we have our User class in its own User.js file
    //and within our 'main.js' file, we have our object instances

    //if we wanted to create a StandardUser Class -- as a Class instantiated from a type of User
         //User -> StandardUser -> derekJimRick

    //derekJimRick will be a StandardUser, an instance from this new Class we are constructing
    //*User.call(this,options) -- calling on User function, creating new instance 
         //'this' now refers to the StandardUser

    function StandardUser (options){  //using our options object above, with all of those parameters
        User.call(this, options) //---HERE!  We are calling the User constructor--which is inheriting User properties
        //we don't want to do 'new User(options) because this won't give us our Standarduser, it will be some random User
        this.freeTierExpiration = Date.now() + 30 //30 days from now, account will expire
        this.referralUser = options.referralUser //creates new 'referralUser' property in our options object
    
 }

 //how do we inherit from the prototype?
    //'FreeUser.prototype = null'

    

